<?php

namespace App\Entity;

use App\Repository\GroupeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GroupeRepository::class)
 */
class Groupe
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $codeGroupe;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $nomGroupe;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $emplacementGroupe;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $numeroSalle;

    /**
     * @ORM\OneToMany(targetEntity=Inscription::class, mappedBy="groupe")
     */
    private $inscriptions;

    /**
     * @ORM\OneToMany(targetEntity=Matiere::class, mappedBy="groupe")
     */
    private $matieres;

    /**
     * @ORM\OneToMany(targetEntity=Reinscription::class, mappedBy="groupe")
     */
    private $reinscriptions;

    public function __construct()
    {
        $this->inscriptions = new ArrayCollection();
        $this->matieres = new ArrayCollection();
        $this->reinscriptions = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->nomGroupe;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeGroupe(): ?string
    {
        return $this->codeGroupe;
    }

    public function setCodeGroupe(string $codeGroupe): self
    {
        $this->codeGroupe = $codeGroupe;

        return $this;
    }

    public function getNomGroupe(): ?string
    {
        return $this->nomGroupe;
    }

    public function setNomGroupe(string $nomGroupe): self
    {
        $this->nomGroupe = $nomGroupe;

        return $this;
    }

    public function getEmplacementGroupe(): ?string
    {
        return $this->emplacementGroupe;
    }

    public function setEmplacementGroupe(string $emplacementGroupe): self
    {
        $this->emplacementGroupe = $emplacementGroupe;

        return $this;
    }

    public function getNumeroSalle(): ?string
    {
        return $this->numeroSalle;
    }

    public function setNumeroSalle(string $numeroSalle): self
    {
        $this->numeroSalle = $numeroSalle;

        return $this;
    }

    /**
     * @return Collection|Inscription[]
     */
    public function getInscriptions(): Collection
    {
        return $this->inscriptions;
    }

    public function addInscription(Inscription $inscription): self
    {
        if (!$this->inscriptions->contains($inscription)) {
            $this->inscriptions[] = $inscription;
            $inscription->setGroupe($this);
        }

        return $this;
    }

    public function removeInscription(Inscription $inscription): self
    {
        if ($this->inscriptions->removeElement($inscription)) {
            // set the owning side to null (unless already changed)
            if ($inscription->getGroupe() === $this) {
                $inscription->setGroupe(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Matiere[]
     */
    public function getMatieres(): Collection
    {
        return $this->matieres;
    }

    public function addMatiere(Matiere $matiere): self
    {
        if (!$this->matieres->contains($matiere)) {
            $this->matieres[] = $matiere;
            $matiere->setGroupe($this);
        }

        return $this;
    }

    public function removeMatiere(Matiere $matiere): self
    {
        if ($this->matieres->removeElement($matiere)) {
            // set the owning side to null (unless already changed)
            if ($matiere->getGroupe() === $this) {
                $matiere->setGroupe(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Reinscription[]
     */
    public function getReinscriptions(): Collection
    {
        return $this->reinscriptions;
    }

    public function addReinscription(Reinscription $reinscription): self
    {
        if (!$this->reinscriptions->contains($reinscription)) {
            $this->reinscriptions[] = $reinscription;
            $reinscription->setGroupe($this);
        }

        return $this;
    }

    public function removeReinscription(Reinscription $reinscription): self
    {
        if ($this->reinscriptions->removeElement($reinscription)) {
            // set the owning side to null (unless already changed)
            if ($reinscription->getGroupe() === $this) {
                $reinscription->setGroupe(null);
            }
        }

        return $this;
    }
}
