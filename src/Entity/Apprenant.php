<?php

namespace App\Entity;

use App\Repository\ApprenantRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ApprenantRepository::class)
 */
class Apprenant
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Inscription::class, inversedBy="apprenants")
     */
    private $nom_app;

    /**
     * @ORM\Column(type="integer")
     */
    private $numero_app;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $photo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomApp(): ?Inscription
    {
        return $this->nom_app;
    }

    public function setNomApp(?Inscription $nom_app): self
    {
        $this->nom_app = $nom_app;

        return $this;
    }

    public function getNumeroApp(): ?int
    {
        return $this->numero_app;
    }

    public function setNumeroApp(int $numero_app): self
    {
        $this->numero_app = $numero_app;

        return $this;
    }

    public function getPhoto()
    {
        return $this->photo;
    }

    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }
}
