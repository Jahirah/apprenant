<?php

namespace App\Entity;

use App\Repository\InscriptionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InscriptionRepository::class)
 */
class Inscription
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $nom_pere;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $fonction_pere;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $prenom_pere;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $nom_mere;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $prenom_mere;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $fonction_mere;

    /**
     * @ORM\Column(type="integer")
     */
    private $immatricule_app;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $nom_app;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $prenom_app;

    /**
     * @ORM\Column(type="date")
     */
    private $date_naissance_app;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $sexe_app;

    /**
     * @ORM\Column(type="text")
     */
    private $adresse_app;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $email_app;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $telephone_app;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $etablissement;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $categorie;

    /**
     * @ORM\Column(type="string", length=11)
     */
    private $annee_universitaire;

    /**
     * @ORM\ManyToOne(targetEntity=Groupe::class, inversedBy="inscriptions")
     */
    private $groupe;

    /**
     * @ORM\OneToMany(targetEntity=Reinscription::class, mappedBy="nom_app")
     */
    private $reinscriptions;

    /**
     * @ORM\OneToMany(targetEntity=Apprenant::class, mappedBy="nom_app")
     */
    private $apprenants;

    /**
     * @ORM\OneToMany(targetEntity=Note::class, mappedBy="nom_app")
     */
    private $notes;

    /**
     * @ORM\OneToMany(targetEntity=Assiduite::class, mappedBy="apprenant")
     */
    private $assiduite_app;

    /**
     * @ORM\Column(type="date")
     */
    private $date_inscription;

    public function __construct()
    {
        $this->reinscriptions = new ArrayCollection();
        $this->apprenants = new ArrayCollection();
        $this->notes = new ArrayCollection();
        $this->assiduite_app = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->nom_app;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomPere(): ?string
    {
        return $this->nom_pere;
    }

    public function setNomPere(string $nom_pere): self
    {
        $this->nom_pere = $nom_pere;

        return $this;
    }

    public function getFonctionPere(): ?string
    {
        return $this->fonction_pere;
    }

    public function setFonctionPere(string $fonction_pere): self
    {
        $this->fonction_pere = $fonction_pere;

        return $this;
    }

    public function getPrenomPere(): ?string
    {
        return $this->prenom_pere;
    }

    public function setPrenomPere(string $prenom_pere): self
    {
        $this->prenom_pere = $prenom_pere;

        return $this;
    }

    public function getNomMere(): ?string
    {
        return $this->nom_mere;
    }

    public function setNomMere(string $nom_mere): self
    {
        $this->nom_mere = $nom_mere;

        return $this;
    }

    public function getPrenomMere(): ?string
    {
        return $this->prenom_mere;
    }

    public function setPrenomMere(string $prenom_mere): self
    {
        $this->prenom_mere = $prenom_mere;

        return $this;
    }

    public function getFonctionMere(): ?string
    {
        return $this->fonction_mere;
    }

    public function setFonctionMere(string $fonction_mere): self
    {
        $this->fonction_mere = $fonction_mere;

        return $this;
    }

    public function getImmatriculeApp(): ?int
    {
        return $this->immatricule_app;
    }

    public function setImmatriculeApp(int $immatricule_app): self
    {
        $this->immatricule_app = $immatricule_app;

        return $this;
    }

    public function getNomApp(): ?string
    {
        return $this->nom_app;
    }

    public function setNomApp(string $nom_app): self
    {
        $this->nom_app = $nom_app;

        return $this;
    }

    public function getPrenomApp(): ?string
    {
        return $this->prenom_app;
    }

    public function setPrenomApp(string $prenom_app): self
    {
        $this->prenom_app = $prenom_app;

        return $this;
    }

    public function getDateNaissanceApp(): ?\DateTimeInterface
    {
        return $this->date_naissance_app;
    }

    public function setDateNaissanceApp(\DateTimeInterface $date_naissance_app): self
    {
        $this->date_naissance_app = $date_naissance_app;

        return $this;
    }

    public function getSexeApp(): ?string
    {
        return $this->sexe_app;
    }

    public function setSexeApp(string $sexe_app): self
    {
        $this->sexe_app = $sexe_app;

        return $this;
    }

    public function getAdresseApp(): ?string
    {
        return $this->adresse_app;
    }

    public function setAdresseApp(string $adresse_app): self
    {
        $this->adresse_app = $adresse_app;

        return $this;
    }

    public function getEmailApp(): ?string
    {
        return $this->email_app;
    }

    public function setEmailApp(string $email_app): self
    {
        $this->email_app = $email_app;

        return $this;
    }

    public function getTelephoneApp(): ?string
    {
        return $this->telephone_app;
    }

    public function setTelephoneApp(string $telephone_app): self
    {
        $this->telephone_app = $telephone_app;

        return $this;
    }

    public function getEtablissement(): ?string
    {
        return $this->etablissement;
    }

    public function setEtablissement(string $etablissement): self
    {
        $this->etablissement = $etablissement;

        return $this;
    }

    public function getCategorie(): ?string
    {
        return $this->categorie;
    }

    public function setCategorie(string $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getAnneeUniversitaire(): ?string
    {
        return $this->annee_universitaire;
    }

    public function setAnneeUniversitaire(string $annee_universitaire): self
    {
        $this->annee_universitaire = $annee_universitaire;

        return $this;
    }

    public function getGroupe(): ?Groupe
    {
        return $this->groupe;
    }

    public function setGroupe(?Groupe $groupe): self
    {
        $this->groupe = $groupe;

        return $this;
    }

    /**
     * @return Collection|Reinscription[]
     */
    public function getReinscriptions(): Collection
    {
        return $this->reinscriptions;
    }

    public function addReinscription(Reinscription $reinscription): self
    {
        if (!$this->reinscriptions->contains($reinscription)) {
            $this->reinscriptions[] = $reinscription;
            $reinscription->setNomApp($this);
        }

        return $this;
    }

    public function removeReinscription(Reinscription $reinscription): self
    {
        if ($this->reinscriptions->removeElement($reinscription)) {
            // set the owning side to null (unless already changed)
            if ($reinscription->getNomApp() === $this) {
                $reinscription->setNomApp(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Apprenant[]
     */
    public function getApprenants(): Collection
    {
        return $this->apprenants;
    }

    public function addApprenant(Apprenant $apprenant): self
    {
        if (!$this->apprenants->contains($apprenant)) {
            $this->apprenants[] = $apprenant;
            $apprenant->setNomApp($this);
        }

        return $this;
    }

    public function removeApprenant(Apprenant $apprenant): self
    {
        if ($this->apprenants->removeElement($apprenant)) {
            // set the owning side to null (unless already changed)
            if ($apprenant->getNomApp() === $this) {
                $apprenant->setNomApp(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Note[]
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    public function addNote(Note $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes[] = $note;
            $note->setNomApp($this);
        }

        return $this;
    }

    public function removeNote(Note $note): self
    {
        if ($this->notes->removeElement($note)) {
            // set the owning side to null (unless already changed)
            if ($note->getNomApp() === $this) {
                $note->setNomApp(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Assiduite[]
     */
    public function getAssiduiteApp(): Collection
    {
        return $this->assiduite_app;
    }

    public function addAssiduiteApp(Assiduite $assiduiteApp): self
    {
        if (!$this->assiduite_app->contains($assiduiteApp)) {
            $this->assiduite_app[] = $assiduiteApp;
            $assiduiteApp->setApprenant($this);
        }

        return $this;
    }

    public function removeAssiduiteApp(Assiduite $assiduiteApp): self
    {
        if ($this->assiduite_app->removeElement($assiduiteApp)) {
            // set the owning side to null (unless already changed)
            if ($assiduiteApp->getApprenant() === $this) {
                $assiduiteApp->setApprenant(null);
            }
        }

        return $this;
    }

    public function getDateInscription(): ?\DateTimeInterface
    {
        return $this->date_inscription;
    }

    public function setDateInscription(\DateTimeInterface $date_inscription): self
    {
        $this->date_inscription = $date_inscription;

        return $this;
    }
}
