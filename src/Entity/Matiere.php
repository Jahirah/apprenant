<?php

namespace App\Entity;

use App\Repository\MatiereRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MatiereRepository::class)
 */
class Matiere
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $semestre;

    /**
     * @ORM\ManyToOne(targetEntity=Groupe::class, inversedBy="matieres")
     */
    private $groupe;

    /**
     * @ORM\ManyToOne(targetEntity=Formateur::class, inversedBy="matieres_formateur")
     */
    private $formateur;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $code_matiere;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom_matiere;

    /**
     * @ORM\Column(type="integer")
     */
    private $coefficient;

    /**
     * @ORM\OneToMany(targetEntity=Note::class, mappedBy="matiere")
     */
    private $notes_matiere;

    /**
     * @ORM\OneToMany(targetEntity=Assiduite::class, mappedBy="matiere")
     */
    private $assiduites;

    public function __construct()
    {
        $this->notes_matiere = new ArrayCollection();
        $this->assiduites = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->nom_matiere;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSemestre(): ?string
    {
        return $this->semestre;
    }

    public function setSemestre(string $semestre): self
    {
        $this->semestre = $semestre;

        return $this;
    }

    public function getGroupe(): ?Groupe
    {
        return $this->groupe;
    }

    public function setGroupe(?Groupe $groupe): self
    {
        $this->groupe = $groupe;

        return $this;
    }

    public function getFormateur(): ?Formateur
    {
        return $this->formateur;
    }

    public function setFormateur(?Formateur $formateur): self
    {
        $this->formateur = $formateur;

        return $this;
    }

    public function getCodeMatiere(): ?string
    {
        return $this->code_matiere;
    }

    public function setCodeMatiere(string $code_matiere): self
    {
        $this->code_matiere = $code_matiere;

        return $this;
    }

    public function getNomMatiere(): ?string
    {
        return $this->nom_matiere;
    }

    public function setNomMatiere(string $nom_matiere): self
    {
        $this->nom_matiere = $nom_matiere;

        return $this;
    }

    public function getCoefficient(): ?int
    {
        return $this->coefficient;
    }

    public function setCoefficient(int $coefficient): self
    {
        $this->coefficient = $coefficient;

        return $this;
    }

    /**
     * @return Collection|Note[]
     */
    public function getNotesMatiere(): Collection
    {
        return $this->notes_matiere;
    }

    public function addNotesMatiere(Note $notesMatiere): self
    {
        if (!$this->notes_matiere->contains($notesMatiere)) {
            $this->notes_matiere[] = $notesMatiere;
            $notesMatiere->setMatiere($this);
        }

        return $this;
    }

    public function removeNotesMatiere(Note $notesMatiere): self
    {
        if ($this->notes_matiere->removeElement($notesMatiere)) {
            // set the owning side to null (unless already changed)
            if ($notesMatiere->getMatiere() === $this) {
                $notesMatiere->setMatiere(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Assiduite[]
     */
    public function getAssiduites(): Collection
    {
        return $this->assiduites;
    }

    public function addAssiduite(Assiduite $assiduite): self
    {
        if (!$this->assiduites->contains($assiduite)) {
            $this->assiduites[] = $assiduite;
            $assiduite->setMatiere($this);
        }

        return $this;
    }

    public function removeAssiduite(Assiduite $assiduite): self
    {
        if ($this->assiduites->removeElement($assiduite)) {
            // set the owning side to null (unless already changed)
            if ($assiduite->getMatiere() === $this) {
                $assiduite->setMatiere(null);
            }
        }

        return $this;
    }
}
