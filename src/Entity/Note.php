<?php

namespace App\Entity;

use App\Repository\NoteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NoteRepository::class)
 */
class Note
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Inscription::class, inversedBy="notes")
     */
    private $nom_app;

    /**
     * @ORM\ManyToOne(targetEntity=Matiere::class, inversedBy="notes_matiere")
     */
    private $matiere;

    /**
     * @ORM\Column(type="float")
     */
    private $note_sc;

    /**
     * @ORM\Column(type="float")
     */
    private $note_ac;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomApp(): ?Inscription
    {
        return $this->nom_app;
    }

    public function setNomApp(?Inscription $nom_app): self
    {
        $this->nom_app = $nom_app;

        return $this;
    }

    public function getMatiere(): ?Matiere
    {
        return $this->matiere;
    }

    public function setMatiere(?Matiere $matiere): self
    {
        $this->matiere = $matiere;

        return $this;
    }

    public function getNoteSc(): ?float
    {
        return $this->note_sc;
    }

    public function setNoteSc(float $note_sc): self
    {
        $this->note_sc = $note_sc;

        return $this;
    }

    public function getNoteAc(): ?float
    {
        return $this->note_ac;
    }

    public function setNoteAc(float $note_ac): self
    {
        $this->note_ac = $note_ac;

        return $this;
    }
}
