<?php

namespace App\Entity;

use App\Repository\ReinscriptionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReinscriptionRepository::class)
 */
class Reinscription
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Inscription::class, inversedBy="reinscriptions")
     */
    private $nom_app;

    /**
     * @ORM\Column(type="string", length=11)
     */
    private $annee_universitaire;

    /**
     * @ORM\Column(type="date")
     */
    private $date_reinscription;

    /**
     * @ORM\ManyToOne(targetEntity=Groupe::class, inversedBy="reinscriptions")
     */
    private $groupe;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomApp(): ?Inscription
    {
        return $this->nom_app;
    }

    public function setNomApp(?Inscription $nom_app): self
    {
        $this->nom_app = $nom_app;

        return $this;
    }

    public function getAnneeUniversitaire(): ?string
    {
        return $this->annee_universitaire;
    }

    public function setAnneeUniversitaire(string $annee_universitaire): self
    {
        $this->annee_universitaire = $annee_universitaire;

        return $this;
    }

    public function getDateReinscription(): ?\DateTimeInterface
    {
        return $this->date_reinscription;
    }

    public function setDateReinscription(\DateTimeInterface $date_reinscription): self
    {
        $this->date_reinscription = $date_reinscription;

        return $this;
    }

    public function getGroupe(): ?Groupe
    {
        return $this->groupe;
    }

    public function setGroupe(?Groupe $groupe): self
    {
        $this->groupe = $groupe;

        return $this;
    }
}
