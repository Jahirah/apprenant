<?php

namespace App\Entity;

use App\Repository\AssiduiteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AssiduiteRepository::class)
 */
class Assiduite
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $heure_cours;

    /**
     * @ORM\ManyToOne(targetEntity=Matiere::class, inversedBy="assiduites")
     */
    private $matiere;

    /**
     * @ORM\ManyToOne(targetEntity=Inscription::class, inversedBy="assiduite_app")
     */
    private $apprenant;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $presence;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getHeureCours(): ?string
    {
        return $this->heure_cours;
    }

    public function setHeureCours(string $heure_cours): self
    {
        $this->heure_cours = $heure_cours;

        return $this;
    }

    public function getMatiere(): ?Matiere
    {
        return $this->matiere;
    }

    public function setMatiere(?Matiere $matiere): self
    {
        $this->matiere = $matiere;

        return $this;
    }

    public function getApprenant(): ?Inscription
    {
        return $this->apprenant;
    }

    public function setApprenant(?Inscription $apprenant): self
    {
        $this->apprenant = $apprenant;

        return $this;
    }

    public function getPresence(): ?string
    {
        return $this->presence;
    }

    public function setPresence(string $presence): self
    {
        $this->presence = $presence;

        return $this;
    }
}
