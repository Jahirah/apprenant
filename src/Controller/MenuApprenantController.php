<?php

namespace App\Controller;

use App\Form\MenuAppType;
use App\Entity\MenuApprenant;
use App\Repository\UserRepository;
use App\Repository\MenuUserRepository;
use App\Repository\MenuVisiteurRepository;
use App\Repository\MenuApprenantRepository;
use App\Repository\MenuFormateurRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\MenuAdministrateurRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/parametrage_role_apprenant")
 */
class MenuApprenantController extends AbstractController
{
    /**
     * @Route("/liste", name="liste")
     */
    public function index(MenuVisiteurRepository $menuVisiteur,MenuUserRepository $menuUser,MenuFormateurRepository $menuFormateur,MenuAdministrateurRepository $menuAdministrateur,MenuApprenantRepository $menuApprenant): Response
    {
        $roleApprenant = 'ROLE_APPRENANT';
        $roleAdmin = 'ROLE_ADMIN';
        $roleFormateur = 'ROLE_FORMATEUR';
        $roleVisiteur = 'ROLE_VISITEUR';
        $roleUtilisateur = 'ROLE_UTILISATEUR';
        $mA = $menuApprenant->findAll();
        $mAd = $menuAdministrateur->findAll();
        $mF = $menuFormateur->findAll();
        $mU = $menuUser->findAll();
        $mV = $menuVisiteur->findAll();
        return $this->render('menu_apprenant/index.html.twig', [
            'menuApprenant' => $menuApprenant->findAll(),
            'menuApprenant' => $mA,
            'menuAdmin' => $mAd,
            'menuFormateur' => $mF,
            'menuUser' => $mU,
            'menuVisiteur' => $mV,
            'roleApprenant' => $roleApprenant,
            'roleAdmin' => $roleAdmin,
            'roleFormateur' => $roleFormateur,
            'roleVisiteur' => $roleVisiteur,
            'roleUtilisateur' => $roleUtilisateur,
        ]);
    }

    /**
     * @Route("/new", name="menu_apprenant_new", methods={"GET","POST"})
     */
    public function new(MenuVisiteurRepository $menuVisiteur,MenuUserRepository $menuUser,MenuFormateurRepository $menuFormateur,MenuAdministrateurRepository $menuAdministrateur,MenuApprenantRepository $menuApprenant,Request $request): Response
    {
        $mApprenant = new MenuApprenant();
        $form = $this->createForm(MenuAppType::class, $mApprenant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($mApprenant);
            $entityManager->flush();
            $this->addFlash(
                'msg',
                'Enregistrer avec succès!'
            );

            return $this->redirectToRoute('liste', [], Response::HTTP_SEE_OTHER);
        }
        $roleApprenant = 'ROLE_APPRENANT';
        $roleAdmin = 'ROLE_ADMIN';
        $roleFormateur = 'ROLE_FORMATEUR';
        $roleVisiteur = 'ROLE_VISITEUR';
        $roleUtilisateur = 'ROLE_UTILISATEUR';
        $mA = $menuApprenant->findAll();
        $mAd = $menuAdministrateur->findAll();
        $mF = $menuFormateur->findAll();
        $mU = $menuUser->findAll();
        $mV = $menuVisiteur->findAll();
        return $this->renderForm('menu_apprenant/add.html.twig', [
            'mApprenant' => $mApprenant,
            'form' => $form,
            'menuApprenant' => $mA,
            'menuAdmin' => $mAd,
            'menuFormateur' => $mF,
            'menuUser' => $mU,
            'menuVisiteur' => $mV,
            'roleApprenant' => $roleApprenant,
            'roleAdmin' => $roleAdmin,
            'roleFormateur' => $roleFormateur,
            'roleVisiteur' => $roleVisiteur,
            'roleUtilisateur' => $roleUtilisateur,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="menu_apprenant_edit", methods={"GET","POST"})
     */
    public function edit(MenuVisiteurRepository $menuVisiteur,MenuUserRepository $menuUser,MenuFormateurRepository $menuFormateur,MenuAdministrateurRepository $menuAdministrateur,MenuApprenantRepository $menuApprenant,Request $request,MenuApprenant $menuApp): Response
    {
        $form = $this->createForm(MenuAppType::class, $menuApp);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash(
                'msg',
                'Modification réussit!'
            );

            return $this->redirectToRoute('liste', [], Response::HTTP_SEE_OTHER);
        }
        $roleApprenant = 'ROLE_APPRENANT';
        $roleAdmin = 'ROLE_ADMIN';
        $roleFormateur = 'ROLE_FORMATEUR';
        $roleVisiteur = 'ROLE_VISITEUR';
        $roleUtilisateur = 'ROLE_UTILISATEUR';
        $mA = $menuApprenant->findAll();
        $mAd = $menuAdministrateur->findAll();
        $mF = $menuFormateur->findAll();
        $mU = $menuUser->findAll();
        $mV = $menuVisiteur->findAll();

        return $this->renderForm('menu_apprenant/edit.html.twig', [
            'menuApprenant' => $menuApp,
            'form' => $form,
            'menuApprenant' => $mA,
            'menuAdmin' => $mAd,
            'menuFormateur' => $mF,
            'menuUser' => $mU,
            'menuVisiteur' => $mV,
            'roleApprenant' => $roleApprenant,
            'roleAdmin' => $roleAdmin,
            'roleFormateur' => $roleFormateur,
            'roleVisiteur' => $roleVisiteur,
            'roleUtilisateur' => $roleUtilisateur,
        ]);
    }

    /**
     * @Route("/{id}", name="menu_apprenant_delete")
     */
    public function delete(MenuApprenant $menuApprenant): RedirectResponse
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($menuApprenant);
        $em->flush();
        return $this->redirectToRoute('liste');
    }

}
