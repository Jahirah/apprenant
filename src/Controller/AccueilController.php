<?php

namespace App\Controller;

use App\Repository\NoteRepository;
use App\Repository\MatiereRepository;
use App\Repository\ApprenantRepository;
use App\Repository\FormateurRepository;
use App\Repository\MenuAdministrateurRepository;
use App\Repository\MenuApprenantRepository;
use App\Repository\MenuFormateurRepository;
use App\Repository\MenuUserRepository;
use App\Repository\MenuVisiteurRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AccueilController extends AbstractController
{
    /**
     * @Route("/accueil", name="accueil")
     */
    public function index(MenuVisiteurRepository $menuVisiteur,MenuUserRepository $menuUser,MenuFormateurRepository $menuFormateur,MenuAdministrateurRepository $menuAdministrateur,MenuApprenantRepository $menuApprenant,NoteRepository $note,ApprenantRepository $apprenant,FormateurRepository $formateur,MatiereRepository $matiere): Response
    {
        $nbApprenant = $apprenant->nombreApprenant();
        if ($nbApprenant == null){
            $nbApprenant = 0;
        }
        $nbFormateur = $formateur->nombreFormateur();
        if ($nbFormateur == null){
            $nbFormateur = 0;
        }
        $nbMatiere = $matiere->nombreMatiere();
        if ($nbMatiere == null){
            $nbMatiere = 0;
        }
        $nbNote = $note->nombreNote();
        if ($nbNote == null){
            $nbNote = 0;
        }
        $roleApprenant = 'ROLE_APPRENANT';
        $roleAdmin = 'ROLE_ADMIN';
        $roleFormateur = 'ROLE_FORMATEUR';
        $roleVisiteur = 'ROLE_VISITEUR';
        $roleUtilisateur = 'ROLE_UTILISATEUR';
        $mA = $menuApprenant->findAll();
        $mAd = $menuAdministrateur->findAll();
        $mF = $menuFormateur->findAll();
        $mU = $menuUser->findAll();
        $mV = $menuVisiteur->findAll();

        return $this->render('accueil/index.html.twig', [
            'nbApprenant' => $nbApprenant,
            'nbFormateur' => $nbFormateur,
            'nbMatiere' => $nbMatiere,
            'nbNote' => $nbNote,
            'menuApprenant' => $mA,
            'menuAdmin' => $mAd,
            'menuFormateur' => $mF,
            'menuUser' => $mU,
            'menuVisiteur' => $mV,
            'roleApprenant' => $roleApprenant,
            'roleAdmin' => $roleAdmin,
            'roleFormateur' => $roleFormateur,
            'roleVisiteur' => $roleVisiteur,
            'roleUtilisateur' => $roleUtilisateur,
        ]);
    }
    
    // public function condition(): Response
    // {
        
    //     return $this->render('include/menu.html.twig', [
            
    //     ]);
    // }
}
