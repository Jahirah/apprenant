<?php

namespace App\Controller;

use Dompdf\Dompdf;
use Dompdf\Options;

use App\Entity\Inscription;
use App\Repository\NoteRepository;
use App\Repository\MenuUserRepository;
use App\Repository\InscriptionRepository;
use App\Repository\MenuVisiteurRepository;
use App\Repository\MenuApprenantRepository;
use App\Repository\MenuFormateurRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\MenuAdministrateurRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

    /**
     * @Route("/evaluation")
     */
class EvaluationController extends AbstractController
{
     
    /**
     * @Route("/", name="evaluation_index")
     * @Method({"GET", "POST"})
     */    
    public function indexSecond(Request $request, NoteRepository $noteRepository, InscriptionRepository $inscriptionRepository,MenuVisiteurRepository $menuVisiteur,MenuUserRepository $menuUser,MenuFormateurRepository $menuFormateur,MenuAdministrateurRepository $menuAdministrateur,MenuApprenantRepository $menuApprenant){
        $repo = $this->getDoctrine()->getRepository(Inscription::class);
        $inscription = $repo->findAll();
        if($request->isXmlHttpRequest()){
            $idApprennant=$request->request->get('id');
            $repositoryApprenant = $noteRepository->noteDunApprenant($idApprennant);
            $sommeNote=$noteRepository->moyenApprenant($idApprennant);
            $coeff = $inscriptionRepository->sommeCoefficient($idApprennant);
            $moyenApprenant= $sommeNote/$coeff;
            $app_moy =  array("apps" => $repositoryApprenant , "moy" => $moyenApprenant);
            return new JsonResponse($app_moy);
        }

        $roleApprenant = 'ROLE_APPRENANT';
        $roleAdmin = 'ROLE_ADMIN';
        $roleFormateur = 'ROLE_FORMATEUR';
        $roleVisiteur = 'ROLE_VISITEUR';
        $roleUtilisateur = 'ROLE_UTILISATEUR';
        $mA = $menuApprenant->findAll();
        $mAd = $menuAdministrateur->findAll();
        $mF = $menuFormateur->findAll();
        $mU = $menuUser->findAll();
        $mV = $menuVisiteur->findAll();

        return $this->render('evaluation/index.html.twig',[
            'inscriptions' => $inscription,
            'menuApprenant' => $mA,
            'menuAdmin' => $mAd,
            'menuFormateur' => $mF,
            'menuUser' => $mU,
            'menuVisiteur' => $mV,
            'roleApprenant' => $roleApprenant,
            'roleAdmin' => $roleAdmin,
            'roleFormateur' => $roleFormateur,
            'roleVisiteur' => $roleVisiteur,
            'roleUtilisateur' => $roleUtilisateur,
        ]);
    }


    /**
     * @Route("/Pdf/{id}", name="evaluationPdf")
     * @Method({"GET", "POST"})
     */
     public function evaluationPDF($id,NoteRepository $noteRepository,InscriptionRepository $inscriptionRepository): Response
     {  
        $repositoryApprenant = $noteRepository->noteDunApprenant($id);
        $firstSem=$noteRepository->premiertrimestre($id);
        $secondSem=$noteRepository->deuxiemetrimestre($id);
        $thirdSem=$noteRepository->troisiemetrimestre($id);
        $sommeNote=$noteRepository->moyenApprenant($id);
        $coeff = $inscriptionRepository->sommeCoefficient($id);
        $moyenApprenant= $sommeNote/$coeff;

        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        $dompdf = new Dompdf($pdfOptions);
        
        $html = $this->renderView('evaluation/evaluationPDF.html.twig',[ 
            'moyen' => $moyenApprenant,
            'Evaluations'=>$repositoryApprenant,
            'firstSem' => $firstSem,
            'secondSem' => $secondSem,
            'thirdSem' => $thirdSem
        ]);

        $dompdf->loadHtml($html);

        $dompdf->setPaper('A4','portrait');

        $dompdf->render();

        $dompdf->stream("EvaluationPDF.pdf", [
            "Attachement" => true
        ]);

        return new Response('', 200, [
            'Content-Type' => 'application/pdf',
          ]);
     }

     /**
     * @Route("/css", name="css")
     */
    public function css(): Response
    {
        return $this->render('evaluation/css.html.twig');
    }

}
