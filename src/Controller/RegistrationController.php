<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Repository\MenuUserRepository;
use App\Repository\MenuVisiteurRepository;
use App\Repository\MenuApprenantRepository;
use App\Repository\MenuFormateurRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\MenuAdministrateurRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
     * @Route("/user")
     */
class RegistrationController extends AbstractController
{
    /**
     * @Route("/new", name="user_new")
     */
    public function register(MenuVisiteurRepository $menuVisiteur,MenuUserRepository $menuUser,MenuFormateurRepository $menuFormateur,MenuAdministrateurRepository $menuAdministrateur,MenuApprenantRepository $menuApprenant,Request $request, UserPasswordHasherInterface $userPasswordHasherInterface): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
            $userPasswordHasherInterface->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            // do anything else you need here, like send an email
            $this->addFlash(
                'msg','Enregistrer avec succès!'
            );

            return $this->redirectToRoute('user_list');
        }
        $roleApprenant = 'ROLE_APPRENANT';
        $roleAdmin = 'ROLE_ADMIN';
        $roleFormateur = 'ROLE_FORMATEUR';
        $roleVisiteur = 'ROLE_VISITEUR';
        $roleUtilisateur = 'ROLE_UTILISATEUR';
        $mA = $menuApprenant->findAll();
        $mAd = $menuAdministrateur->findAll();
        $mF = $menuFormateur->findAll();
        $mU = $menuUser->findAll();
        $mV = $menuVisiteur->findAll();

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
            'menuApprenant' => $mA,
            'menuAdmin' => $mAd,
            'menuFormateur' => $mF,
            'menuUser' => $mU,
            'menuVisiteur' => $mV,
            'roleApprenant' => $roleApprenant,
            'roleAdmin' => $roleAdmin,
            'roleFormateur' => $roleFormateur,
            'roleVisiteur' => $roleVisiteur,
            'roleUtilisateur' => $roleUtilisateur,
        ]);
    }

    // Liste des utilisateurs
    /**
     * @Route("/", name="user_list")
     */
    public function listeUser(MenuVisiteurRepository $menuVisiteur,MenuUserRepository $menuUser,MenuFormateurRepository $menuFormateur,MenuAdministrateurRepository $menuAdministrateur,MenuApprenantRepository $menuApprenant): Response
    {
        $depot = $this->getDoctrine()->getRepository(User::class);
        $listeUser = $depot->findAll();
        $roleApprenant = 'ROLE_APPRENANT';
        $roleAdmin = 'ROLE_ADMIN';
        $roleFormateur = 'ROLE_FORMATEUR';
        $roleVisiteur = 'ROLE_VISITEUR';
        $roleUtilisateur = 'ROLE_UTILISATEUR';
        $mA = $menuApprenant->findAll();
        $mAd = $menuAdministrateur->findAll();
        $mF = $menuFormateur->findAll();
        $mU = $menuUser->findAll();
        $mV = $menuVisiteur->findAll();

        return $this->render('registration/liste.html.twig', [
            'listeUser' => $listeUser,
            'menuApprenant' => $mA,
            'menuAdmin' => $mAd,
            'menuFormateur' => $mF,
            'menuUser' => $mU,
            'menuVisiteur' => $mV,
            'roleApprenant' => $roleApprenant,
            'roleAdmin' => $roleAdmin,
            'roleFormateur' => $roleFormateur,
            'roleVisiteur' => $roleVisiteur,
            'roleUtilisateur' => $roleUtilisateur,
        ]);
    }

    // Modifier un utilisateur
    /**
     * @Route("/edit/{id}", name="user_edit")
     */

    public function editerUtilisateur(MenuVisiteurRepository $menuVisiteur,MenuUserRepository $menuUser,MenuFormateurRepository $menuFormateur,MenuAdministrateurRepository $menuAdministrateur,MenuApprenantRepository $menuApprenant,User $user, UserPasswordEncoderInterface $passwordEncoder, Request $request): Response
    {
        $formulaire = $this->createForm(RegistrationFormType::class, $user);
        $formulaire->handleRequest($request);
        if ($formulaire->isSubmitted() && $formulaire->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $formulaire->get('plainPassword')->getData()
                )
            );

            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $this->addFlash(
                'msg','Modification réussit!'
            );
            return $this->redirectToRoute('user_list');
        }
        $roleApprenant = 'ROLE_APPRENANT';
        $roleAdmin = 'ROLE_ADMIN';
        $roleFormateur = 'ROLE_FORMATEUR';
        $roleVisiteur = 'ROLE_VISITEUR';
        $roleUtilisateur = 'ROLE_UTILISATEUR';
        $mA = $menuApprenant->findAll();
        $mAd = $menuAdministrateur->findAll();
        $mF = $menuFormateur->findAll();
        $mU = $menuUser->findAll();
        $mV = $menuVisiteur->findAll();

        return $this->render('registration/edit.html.twig', [
            'registrationForm' => $formulaire->createView(),
            'menuApprenant' => $mA,
            'menuAdmin' => $mAd,
            'menuFormateur' => $mF,
            'menuUser' => $mU,
            'menuVisiteur' => $mV,
            'roleApprenant' => $roleApprenant,
            'roleAdmin' => $roleAdmin,
            'roleFormateur' => $roleFormateur,
            'roleVisiteur' => $roleVisiteur,
            'roleUtilisateur' => $roleUtilisateur,
        ]);
    }

    // Supprimer un utilisateur

    /**
     * @Route("/delete/{id}", name="user_delete")
     */

    public function supprimerNote(User $user): RedirectResponse
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();
        return $this->redirectToRoute('user_list');
    }

}
