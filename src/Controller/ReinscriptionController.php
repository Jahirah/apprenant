<?php

namespace App\Controller;

use App\Entity\Reinscription;
use App\Form\ReinscriptionType;
use App\Repository\MenuUserRepository;
use App\Repository\MenuVisiteurRepository;
use App\Repository\MenuApprenantRepository;
use App\Repository\MenuFormateurRepository;
use App\Repository\ReinscriptionRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\MenuAdministrateurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/reinscription")
 */
class ReinscriptionController extends AbstractController
{
    /**
     * @Route("/", name="reinscription_index", methods={"GET"})
     */
    public function index(MenuVisiteurRepository $menuVisiteur,MenuUserRepository $menuUser,MenuFormateurRepository $menuFormateur,MenuAdministrateurRepository $menuAdministrateur,MenuApprenantRepository $menuApprenant,ReinscriptionRepository $reinscriptionRepository): Response
    {
        $roleApprenant = 'ROLE_APPRENANT';
        $roleAdmin = 'ROLE_ADMIN';
        $roleFormateur = 'ROLE_FORMATEUR';
        $roleVisiteur = 'ROLE_VISITEUR';
        $roleUtilisateur = 'ROLE_UTILISATEUR';
        $mA = $menuApprenant->findAll();
        $mAd = $menuAdministrateur->findAll();
        $mF = $menuFormateur->findAll();
        $mU = $menuUser->findAll();
        $mV = $menuVisiteur->findAll();
       
        return $this->render('reinscription/index.html.twig', [
            'reinscriptions' => $reinscriptionRepository->findAll(),
            'menuApprenant' => $mA,
            'menuAdmin' => $mAd,
            'menuFormateur' => $mF,
            'menuUser' => $mU,
            'menuVisiteur' => $mV,
            'roleApprenant' => $roleApprenant,
            'roleAdmin' => $roleAdmin,
            'roleFormateur' => $roleFormateur,
            'roleVisiteur' => $roleVisiteur,
            'roleUtilisateur' => $roleUtilisateur,
        ]);
    }

    /**
     * @Route("/new", name="reinscription_new", methods={"GET","POST"})
     */
    public function new(MenuVisiteurRepository $menuVisiteur,MenuUserRepository $menuUser,MenuFormateurRepository $menuFormateur,MenuAdministrateurRepository $menuAdministrateur,MenuApprenantRepository $menuApprenant,Request $request): Response
    {
        $reinscription = new Reinscription();
        $form = $this->createForm(ReinscriptionType::class, $reinscription);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($reinscription);
            $entityManager->flush();
            $this->addFlash(
                'msg','Enregistrer avec succès'
            );

            return $this->redirectToRoute('reinscription_index', [], Response::HTTP_SEE_OTHER);
        }
        $roleApprenant = 'ROLE_APPRENANT';
        $roleAdmin = 'ROLE_ADMIN';
        $roleFormateur = 'ROLE_FORMATEUR';
        $roleVisiteur = 'ROLE_VISITEUR';
        $roleUtilisateur = 'ROLE_UTILISATEUR';
        $mA = $menuApprenant->findAll();
        $mAd = $menuAdministrateur->findAll();
        $mF = $menuFormateur->findAll();
        $mU = $menuUser->findAll();
        $mV = $menuVisiteur->findAll();

        return $this->renderForm('reinscription/new.html.twig', [
            'reinscription' => $reinscription,
            'form' => $form,
            'menuApprenant' => $mA,
            'menuAdmin' => $mAd,
            'menuFormateur' => $mF,
            'menuUser' => $mU,
            'menuVisiteur' => $mV,
            'roleApprenant' => $roleApprenant,
            'roleAdmin' => $roleAdmin,
            'roleFormateur' => $roleFormateur,
            'roleVisiteur' => $roleVisiteur,
            'roleUtilisateur' => $roleUtilisateur,
        ]);
    }

    /**
     * @Route("/{id}", name="reinscription_show", methods={"GET"})
     */
    public function show(MenuVisiteurRepository $menuVisiteur,MenuUserRepository $menuUser,MenuFormateurRepository $menuFormateur,MenuAdministrateurRepository $menuAdministrateur,MenuApprenantRepository $menuApprenant,Reinscription $reinscription): Response
    {
        $roleApprenant = 'ROLE_APPRENANT';
        $roleAdmin = 'ROLE_ADMIN';
        $roleFormateur = 'ROLE_FORMATEUR';
        $roleVisiteur = 'ROLE_VISITEUR';
        $roleUtilisateur = 'ROLE_UTILISATEUR';
        $mA = $menuApprenant->findAll();
        $mAd = $menuAdministrateur->findAll();
        $mF = $menuFormateur->findAll();
        $mU = $menuUser->findAll();
        $mV = $menuVisiteur->findAll();
        return $this->render('reinscription/show.html.twig', [
            'reinscription' => $reinscription,
            'menuApprenant' => $mA,
            'menuAdmin' => $mAd,
            'menuFormateur' => $mF,
            'menuUser' => $mU,
            'menuVisiteur' => $mV,
            'roleApprenant' => $roleApprenant,
            'roleAdmin' => $roleAdmin,
            'roleFormateur' => $roleFormateur,
            'roleVisiteur' => $roleVisiteur,
            'roleUtilisateur' => $roleUtilisateur,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="reinscription_edit", methods={"GET","POST"})
     */
    public function edit(MenuVisiteurRepository $menuVisiteur,MenuUserRepository $menuUser,MenuFormateurRepository $menuFormateur,MenuAdministrateurRepository $menuAdministrateur,MenuApprenantRepository $menuApprenant,Request $request, Reinscription $reinscription): Response
    {
        $form = $this->createForm(ReinscriptionType::class, $reinscription);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash(
                'msg','Modification réussit!'
            );

            return $this->redirectToRoute('reinscription_index', [], Response::HTTP_SEE_OTHER);
        }
        $roleApprenant = 'ROLE_APPRENANT';
        $roleAdmin = 'ROLE_ADMIN';
        $roleFormateur = 'ROLE_FORMATEUR';
        $roleVisiteur = 'ROLE_VISITEUR';
        $roleUtilisateur = 'ROLE_UTILISATEUR';
        $mA = $menuApprenant->findAll();
        $mAd = $menuAdministrateur->findAll();
        $mF = $menuFormateur->findAll();
        $mU = $menuUser->findAll();
        $mV = $menuVisiteur->findAll();

        return $this->renderForm('reinscription/edit.html.twig', [
            'reinscription' => $reinscription,
            'form' => $form,
            'menuApprenant' => $mA,
            'menuAdmin' => $mAd,
            'menuFormateur' => $mF,
            'menuUser' => $mU,
            'menuVisiteur' => $mV,
            'roleApprenant' => $roleApprenant,
            'roleAdmin' => $roleAdmin,
            'roleFormateur' => $roleFormateur,
            'roleVisiteur' => $roleVisiteur,
            'roleUtilisateur' => $roleUtilisateur,
        ]);
    }

    /**
     * @Route("/{id}", name="reinscription_delete", methods={"POST"})
     */
    public function delete(Request $request, Reinscription $reinscription): Response
    {
        if ($this->isCsrfTokenValid('delete'.$reinscription->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($reinscription);
            $entityManager->flush();
        }

        return $this->redirectToRoute('reinscription_index', [], Response::HTTP_SEE_OTHER);
    }
}
