<?php

namespace App\Controller;

use App\Entity\Inscription;
use App\Form\InscriptionType;
use App\Repository\MenuUserRepository;
use App\Repository\InscriptionRepository;
use App\Repository\MenuVisiteurRepository;
use App\Repository\MenuApprenantRepository;
use App\Repository\MenuFormateurRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\MenuAdministrateurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/inscription")
 */
class InscriptionController extends AbstractController
{
    /**
     * @Route("/", name="inscription_index", methods={"GET"})
     */
    public function index(MenuVisiteurRepository $menuVisiteur,MenuUserRepository $menuUser,MenuFormateurRepository $menuFormateur,MenuAdministrateurRepository $menuAdministrateur,MenuApprenantRepository $menuApprenant,InscriptionRepository $inscriptionRepository): Response
    {
        $roleApprenant = 'ROLE_APPRENANT';
        $roleAdmin = 'ROLE_ADMIN';
        $roleFormateur = 'ROLE_FORMATEUR';
        $roleVisiteur = 'ROLE_VISITEUR';
        $roleUtilisateur = 'ROLE_UTILISATEUR';
        $mA = $menuApprenant->findAll();
        $mAd = $menuAdministrateur->findAll();
        $mF = $menuFormateur->findAll();
        $mU = $menuUser->findAll();
        $mV = $menuVisiteur->findAll();
        return $this->render('inscription/index.html.twig', [
            'inscriptions' => $inscriptionRepository->findAll(),
            'menuApprenant' => $mA,
            'menuAdmin' => $mAd,
            'menuFormateur' => $mF,
            'menuUser' => $mU,
            'menuVisiteur' => $mV,
            'roleApprenant' => $roleApprenant,
            'roleAdmin' => $roleAdmin,
            'roleFormateur' => $roleFormateur,
            'roleVisiteur' => $roleVisiteur,
            'roleUtilisateur' => $roleUtilisateur,
        ]);
    }

    /**
     * @Route("/new", name="inscription_new", methods={"GET","POST"})
     */
    public function new(MenuVisiteurRepository $menuVisiteur,MenuUserRepository $menuUser,MenuFormateurRepository $menuFormateur,MenuAdministrateurRepository $menuAdministrateur,MenuApprenantRepository $menuApprenant,Request $request): Response
    {
        $inscription = new Inscription();
        $form = $this->createForm(InscriptionType::class, $inscription);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($inscription);
            $entityManager->flush();
            $this->addFlash(
                'msg','Enregistrer avec succès!'
            );

            return $this->redirectToRoute('inscription_index', [], Response::HTTP_SEE_OTHER);
        }
        $roleApprenant = 'ROLE_APPRENANT';
        $roleAdmin = 'ROLE_ADMIN';
        $roleFormateur = 'ROLE_FORMATEUR';
        $roleVisiteur = 'ROLE_VISITEUR';
        $roleUtilisateur = 'ROLE_UTILISATEUR';
        $mA = $menuApprenant->findAll();
        $mAd = $menuAdministrateur->findAll();
        $mF = $menuFormateur->findAll();
        $mU = $menuUser->findAll();
        $mV = $menuVisiteur->findAll();

        return $this->renderForm('inscription/new.html.twig', [
            'inscription' => $inscription,
            'form' => $form,
            'menuApprenant' => $mA,
            'menuAdmin' => $mAd,
            'menuFormateur' => $mF,
            'menuUser' => $mU,
            'menuVisiteur' => $mV,
            'roleApprenant' => $roleApprenant,
            'roleAdmin' => $roleAdmin,
            'roleFormateur' => $roleFormateur,
            'roleVisiteur' => $roleVisiteur,
            'roleUtilisateur' => $roleUtilisateur,
        ]);
    }

    /**
     * @Route("/{id}", name="inscription_show", methods={"GET"})
     */
    public function show(MenuVisiteurRepository $menuVisiteur,MenuUserRepository $menuUser,MenuFormateurRepository $menuFormateur,MenuAdministrateurRepository $menuAdministrateur,MenuApprenantRepository $menuApprenant,Inscription $inscription): Response
    {
        $roleApprenant = 'ROLE_APPRENANT';
        $roleAdmin = 'ROLE_ADMIN';
        $roleFormateur = 'ROLE_FORMATEUR';
        $roleVisiteur = 'ROLE_VISITEUR';
        $roleUtilisateur = 'ROLE_UTILISATEUR';
        $mA = $menuApprenant->findAll();
        $mAd = $menuAdministrateur->findAll();
        $mF = $menuFormateur->findAll();
        $mU = $menuUser->findAll();
        $mV = $menuVisiteur->findAll();
        return $this->render('inscription/show.html.twig', [
            'inscription' => $inscription,
            'menuApprenant' => $mA,
            'menuAdmin' => $mAd,
            'menuFormateur' => $mF,
            'menuUser' => $mU,
            'menuVisiteur' => $mV,
            'roleApprenant' => $roleApprenant,
            'roleAdmin' => $roleAdmin,
            'roleFormateur' => $roleFormateur,
            'roleVisiteur' => $roleVisiteur,
            'roleUtilisateur' => $roleUtilisateur,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="inscription_edit", methods={"GET","POST"})
     */
    public function edit(MenuVisiteurRepository $menuVisiteur,MenuUserRepository $menuUser,MenuFormateurRepository $menuFormateur,MenuAdministrateurRepository $menuAdministrateur,MenuApprenantRepository $menuApprenant,Request $request, Inscription $inscription): Response
    {
        $form = $this->createForm(InscriptionType::class, $inscription);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash(
                'msg','Modification réussit!'
            );

            return $this->redirectToRoute('inscription_index', [], Response::HTTP_SEE_OTHER);
        }
        $roleApprenant = 'ROLE_APPRENANT';
        $roleAdmin = 'ROLE_ADMIN';
        $roleFormateur = 'ROLE_FORMATEUR';
        $roleVisiteur = 'ROLE_VISITEUR';
        $roleUtilisateur = 'ROLE_UTILISATEUR';
        $mA = $menuApprenant->findAll();
        $mAd = $menuAdministrateur->findAll();
        $mF = $menuFormateur->findAll();
        $mU = $menuUser->findAll();
        $mV = $menuVisiteur->findAll();

        return $this->renderForm('inscription/edit.html.twig', [
            'inscription' => $inscription,
            'form' => $form,
            'menuApprenant' => $mA,
            'menuAdmin' => $mAd,
            'menuFormateur' => $mF,
            'menuUser' => $mU,
            'menuVisiteur' => $mV,
            'roleApprenant' => $roleApprenant,
            'roleAdmin' => $roleAdmin,
            'roleFormateur' => $roleFormateur,
            'roleVisiteur' => $roleVisiteur,
            'roleUtilisateur' => $roleUtilisateur,
        ]);
    }

    /**
     * @Route("/{id}", name="inscription_delete", methods={"POST"})
     */
    public function delete(Request $request, Inscription $inscription): Response
    {
        if ($this->isCsrfTokenValid('delete'.$inscription->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($inscription);
            $entityManager->flush();
        }

        return $this->redirectToRoute('inscription_index', [], Response::HTTP_SEE_OTHER);
    }
}
