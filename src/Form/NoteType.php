<?php

namespace App\Form;

use App\Entity\Note;
use App\Entity\Matiere;
use App\Entity\Inscription;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class NoteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('nom_app', EntityType::class, [
            'placeholder' => 'Veuillez choisir',
            "attr" => array (
                "class" => "form-control"
             ),
            'class' => Inscription::class,
            'query_builder' => function(EntityRepository $apprenant){
                return $apprenant->createQueryBuilder('a')
                        ->orderBy('a.nom_app', 'ASC');
            }
        ])
            ->add('matiere', EntityType::class, [
                'placeholder' => 'Veuillez choisir',
                "attr" => array (
                    "class" => "form-control"
                 ),
                'class' => Matiere::class,
                'query_builder' => function(EntityRepository $matiere){
                    return $matiere->createQueryBuilder('m')
                            ->orderBy('m.nom_matiere', 'ASC');
                }
            ])
            ->add('note_sc', TextType::class, [
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                )
            ])
            // ->add('note_ac', TextType::class, [
            //     "attr" => array (
            //         "class" => "form-control",
            //         'required'   => true,
            //     )
            // ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Note::class,
        ]);
    }
}
