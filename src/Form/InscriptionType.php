<?php

namespace App\Form;

use App\Entity\Groupe;
use App\Entity\Inscription;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class InscriptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom_pere', TextType::class, [
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                )
            ])
            ->add('prenom_pere', TextType::class, [
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                )
            ])
            ->add('fonction_pere', TextType::class, [
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                )
            ])
            ->add('nom_mere', TextType::class, [
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                )
            ])
            ->add('prenom_mere', TextType::class, [
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                )
            ])
            ->add('fonction_mere', TextType::class, [
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                )
            ])
            ->add('immatricule_app', TextType::class, [
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                )
            ])
            ->add('nom_app', TextType::class, [
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                )
            ])
            ->add('prenom_app', TextType::class, [
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                )
            ])
            ->add('date_naissance_app', DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                "attr" => array (
                    "class" => "form-control",                        
                    'required'   => true,                    
                )
            ])
            ->add('sexe_app', ChoiceType::class, [
                'placeholder' => 'Veuillez choisir',
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                ),
                'choices' => [
                    'Masculin' => 'Masculin',
                    'Féminin' => 'Féminin',
                ],
            ])
            ->add('adresse_app', TextareaType::class, [
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                )
            ])
            ->add('email_app', EmailType::class, [
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                )
            ])
            ->add('telephone_app', TextType::class, [
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                )
            ])
            ->add('etablissement', TextType::class, [
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                )
            ])
            ->add('categorie', ChoiceType::class, [
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                ),
                'choices' => [
                    'Nouveau' => 'Nouveau',
                    'Transfert' => 'Transfert',
                ],
            ])
            ->add('annee_universitaire', TextType::class, [
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                )
            ])
            ->add('groupe', EntityType::class, [
                'placeholder' => 'Veuillez choisir',
                "attr" => array (
                    "class" => "form-control"
                 ),
                'class' => Groupe::class,
                'query_builder' => function(EntityRepository $groupe){
                    return $groupe->createQueryBuilder('g')
                            ->orderBy('g.nomGroupe', 'ASC');
                }
            ])
            ->add('date_inscription', DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                "attr" => array (
                    "class" => "form-control",                        
                    'required'   => true,                    
                )
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Inscription::class,
        ]);
    }
}
