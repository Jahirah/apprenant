<?php

namespace App\Form;

use App\Entity\MenuFormateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class MenuFormateurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        // ->add('libelle', ChoiceType::class, [
        //     "attr" => array (
        //         "class" => "form-control",
        //         'required'   => true,
        //     ),
        //     'choices' => [
        //         'Inscription' => 'Inscription',
        //             'Reinscription' => 'Reinscription',
        //             'Apprenant' => 'Apprenant',
        //             'Assuidite' => 'Assuidite',
        //             'Evaluation' => 'Evaluation',
        //             'Formateur' => 'Formateur',
        //             'Groupe' => 'Groupe',
        //             'Matiere' => 'Matiere',
        //             'Motivation' => 'Motivation',
        //             'Note' => 'Note',
        //     ],
        // ])
        ->add ('libelle', TextType::class)
        ->add('affichage', ChoiceType::class, [
            "attr" => array (
                "class" => "form-control",
                'required'   => true,
            ),
            'choices' => [
                'Affiché' => 'Affiché',
                'Caché' => 'Caché',
            ],
        ])
        ->add('activation', ChoiceType::class, [
            "attr" => array (
                "class" => "form-control",
                'required'   => true,
            ),
            'choices' => [
                'Activé' => 'Activé',
                'Bloqué' => 'Bloqué',
                'Aucun' => 'Aucun',
            ],
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MenuFormateur::class,
        ]);
    }
}
