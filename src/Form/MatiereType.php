<?php

namespace App\Form;

use App\Entity\Groupe;
use App\Entity\Matiere;
use App\Entity\Formateur;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class MatiereType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('semestre', TextType::class, [
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                )
            ])
            ->add('groupe', EntityType::class, [
                'placeholder' => 'Veuillez choisir',
                "attr" => array (
                    "class" => "form-control"
                 ),
                'class' => Groupe::class,
                'query_builder' => function(EntityRepository $groupe){
                    return $groupe->createQueryBuilder('g')
                            ->orderBy('g.nomGroupe', 'ASC');
                }
            ])
            ->add('code_matiere', TextType::class, [
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                )
            ])
            ->add('nom_matiere', TextType::class, [
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                )
            ])
            ->add('coefficient', TextType::class, [
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                )
            ])
            ->add('formateur', EntityType::class, [
                'placeholder' => 'Veuillez choisir',
                "attr" => array (
                    "class" => "form-control"
                 ),
                'class' => Formateur::class,
                'query_builder' => function(EntityRepository $formateur){
                    return $formateur->createQueryBuilder('f')
                            ->orderBy('f.nom', 'ASC');
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Matiere::class,
        ]);
    }
}
