<?php

namespace App\Form;

use App\Entity\Groupe;
use App\Entity\Inscription;
use App\Entity\Reinscription;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ReinscriptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom_app', EntityType::class, [
                'placeholder' => 'Veuillez choisir',
                "attr" => array (
                    "class" => "form-control"
                 ),
                'class' => Inscription::class,
                'query_builder' => function(EntityRepository $apprenant){
                    return $apprenant->createQueryBuilder('a')
                            ->orderBy('a.nom_app', 'ASC');
                }
            ])
            ->add('date_reinscription', DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                "attr" => array (
                    "class" => "form-control",                        
                    'required'   => true,                    
                )
            ])
            ->add('groupe', EntityType::class, [
                'placeholder' => 'Veuillez choisir',
                "attr" => array (
                    "class" => "form-control"
                 ),
                'class' => Groupe::class,
                'query_builder' => function(EntityRepository $groupe){
                    return $groupe->createQueryBuilder('g')
                            ->orderBy('g.nomGroupe', 'ASC');
                }
            ])
            ->add('annee_universitaire', TextType::class, [
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                )
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reinscription::class,
        ]);
    }
}
