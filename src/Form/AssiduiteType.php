<?php

namespace App\Form;

use App\Entity\Matiere;
use App\Entity\Assiduite;
use App\Entity\Inscription;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class AssiduiteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('date', DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                "attr" => array (
                    "class" => "form-control",                        
                    'required'   => true,                    
                )
            ])
            ->add('heure_cours', TextType::class, [
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                )
            ])
            ->add('matiere', EntityType::class, [
                'placeholder' => 'Veuillez choisir',
                "attr" => array (
                    "class" => "form-control"
                 ),
                'class' => Matiere::class,
                'query_builder' => function(EntityRepository $matiere){
                    return $matiere->createQueryBuilder('m')
                            ->orderBy('m.nom_matiere', 'ASC');
                }
            ])
            ->add('apprenant', EntityType::class, [
                'placeholder' => 'Veuillez choisir',
                "attr" => array (
                    "class" => "form-control"
                 ),
                'class' => Inscription::class,
                'query_builder' => function(EntityRepository $apprenant){
                    return $apprenant->createQueryBuilder('a')
                            ->orderBy('a.nom_app', 'ASC');
                }
            ])
            ->add('presence', ChoiceType::class, [
                'placeholder' => 'Veuillez choisir',
                "attr" => array (
                    "class" => "form-control",
                    'required'   => true,
                ),
                'choices' => [
                    'Absent' => 'Absent',
                    'Present' => 'Present',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Assiduite::class,
        ]);
    }
}
