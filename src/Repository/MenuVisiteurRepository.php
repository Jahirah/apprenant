<?php

namespace App\Repository;

use App\Entity\MenuVisiteur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MenuVisiteur|null find($id, $lockMode = null, $lockVersion = null)
 * @method MenuVisiteur|null findOneBy(array $criteria, array $orderBy = null)
 * @method MenuVisiteur[]    findAll()
 * @method MenuVisiteur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MenuVisiteurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MenuVisiteur::class);
    }

    // /**
    //  * @return MenuVisiteur[] Returns an array of MenuVisiteur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MenuVisiteur
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
