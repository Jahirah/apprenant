<?php

namespace App\Repository;

use App\Entity\MenuApprenant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MenuApprenant|null find($id, $lockMode = null, $lockVersion = null)
 * @method MenuApprenant|null findOneBy(array $criteria, array $orderBy = null)
 * @method MenuApprenant[]    findAll()
 * @method MenuApprenant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MenuApprenantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MenuApprenant::class);
    }
    
    public function rechercheApprenant($mApprenant)
    {
        return $this->createQueryBuilder('a')
            ->where('a.libelle = :val')
            ->setParameter('val', $mApprenant)
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return MenuApprenant[] Returns an array of MenuApprenant objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MenuApprenant
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
