<?php

namespace App\Repository;

use App\Entity\MenuAdministrateur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MenuAdministrateur|null find($id, $lockMode = null, $lockVersion = null)
 * @method MenuAdministrateur|null findOneBy(array $criteria, array $orderBy = null)
 * @method MenuAdministrateur[]    findAll()
 * @method MenuAdministrateur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MenuAdministrateurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MenuAdministrateur::class);
    }

    // /**
    //  * @return MenuAdministrateur[] Returns an array of MenuAdministrateur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MenuAdministrateur
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
