<?php

namespace App\Repository;

use App\Entity\Reinscription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Reinscription|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reinscription|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reinscription[]    findAll()
 * @method Reinscription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReinscriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reinscription::class);
    }

    // /**
    //  * @return Reinscription[] Returns an array of Reinscription objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Reinscription
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
