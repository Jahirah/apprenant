<?php

namespace App\Repository;

use App\Entity\Note;
use App\Entity\Inscription;
use App\Entity\Matiere;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Note|null find($id, $lockMode = null, $lockVersion = null)
 * @method Note|null findOneBy(array $criteria, array $orderBy = null)
 * @method Note[]    findAll()
 * @method Note[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Note::class);
    }

    public function noteDunApprenant($id){
        
        $query=$this->getEntityManager()->createQuery("select m.semestre as periode, m.nom_matiere as matiere, n.note_sc as noteS, 
        m.coefficient as coefficient, i.nom_app as nom, i.prenom_app as prenom FROM
        App\Entity\Inscription i, App\Entity\Note n, App\Entity\Matiere m where (i.id=n.nom_app) AND
         (m.id=n.matiere) AND i.id = :id")
        ->setParameter("id", $id);
        return $query->getResult();
    }


    public function moyenApprenant ($nom){
        return $this->createQueryBuilder('n')
        ->select(" sum(n.note_ac) as note")
        ->where('n.nom_app = :name')
        ->setParameter("name", $nom)
        ->getQuery()
        ->getSingleScalarResult();
    }

    public function premiertrimestre ($nom){
        return $this->createQueryBuilder('n')
        ->join('n.matiere','m')
        ->select("count(n.id) as nombrePremier")
        ->where('n.nom_app = :name')
        ->andWhere('m.semestre = :sem')
        ->setParameter("name", $nom)
        ->setParameter("sem", "1er trimestre")
        ->getQuery()
        ->getSingleScalarResult();
    }

    public function deuxiemetrimestre ($nom){
        return $this->createQueryBuilder('n')
        ->join('n.matiere','m')
        ->select("count(n.id) as nombreDeuxieme")
        ->where('n.nom_app = :name')
        ->andWhere('m.semestre = :sem')
        ->setParameter("name", $nom)
        ->setParameter("sem", "2em trimestre")
        ->getQuery()
        ->getSingleScalarResult();
    }

    public function troisiemetrimestre ($nom){
        return $this->createQueryBuilder('n')
        ->join('n.matiere','m')
        ->select("count(n.id) as nombreTroisieme")
        ->where('n.nom_app = :name')
        ->andWhere('m.semestre = :sem')
        ->setParameter("name", $nom)
        ->setParameter("sem", "3em trimestre")
        ->getQuery()
        ->getSingleScalarResult();
    }

    public function nombreNote()
    {
        return $this->createQueryBuilder('n')
            ->select('count(n.id)')
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    // /**
    //  * @return Note[] Returns an array of Note objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Note
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
